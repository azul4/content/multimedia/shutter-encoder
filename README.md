# shutter-encoder

Converter for all formats video|audio|image professionnals codecs and standards - swiss knife tool for Linux

https://www.shutterencoder.com/

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/shutter-encoder.git
```
